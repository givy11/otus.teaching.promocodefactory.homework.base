﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = (IList<T>)data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Create(T record)
        {
            record.Id = Guid.NewGuid();
            Data.Add(record);
            return GetByIdAsync(record.Id);
        }

        public Task<T> Update(T record)
        {
            var current = Data.FirstOrDefault(x => x.Id == record.Id);
            if (current != null)
            {
                var index = Data.IndexOf(current);
                Data[index] = record;
            }
            return GetByIdAsync(record.Id);
        }

        public Task Delete(Guid recordId)
        {
            Data = Data.Where(x=>x.Id != recordId).ToList();
            return Task.CompletedTask;
        }
    }
}